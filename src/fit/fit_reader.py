'''
Created on 18.02.2014

@author: christian
'''

import struct

from fit.fit_const import getTypeDef, getMessageDef, convertFitBaseType


# Fit file structure: FileHeader
class FileHeader:
    def __init__(self, f):
        self.headerSize = struct.unpack('B', f.read(1))[0]
        self.protocolVersion = struct.unpack('B', f.read(1))[0]
        self.profileVersion = struct.unpack('<H', f.read(2))[0]
        self.dataSize = struct.unpack('<L', f.read(4))[0]
        self.dataType = f.read(4)
        if self.headerSize > 12:
            self.crc = struct.unpack('<H', f.read(2))[0]
        else:
            self.crc = 0
            
    def __repr__(self):
        ret = self.__class__.__name__ + "["
        for entry in self.__dict__:
            ret += ";" + entry + "=" + str(self.__dict__[entry]) 
        return ret + "]"
        

# Fit file structure: RecordHeader
class RecordHeader:
    def __init__(self, f):
        nextFromFile = f.read(1)
        self.endOfFile = nextFromFile == ''
        if self.endOfFile:
            return
        
        byte = ord(nextFromFile)
        
        bit7 = (byte & 2 ** 7) != 0
        self.isNormalHeader = not(bit7)
        self.isCompressedTimestampHeader = bit7
        
        bit6 = (byte & 2 ** 6) != 0
        self.isDefinitionMessage = bit6
        self.isDataMessage = not(bit6)
        
        if self.isNormalHeader:
            self.localMessageType = byte & 15
             
        if self.isCompressedTimestampHeader:
            self.localMessageType = (byte << 5) & 3 
            self.timeOffset = byte & 31

    def __repr__(self):
        ret = self.__class__.__name__ + "["
        for entry in self.__dict__:
            ret += ";" + entry + "=" + str(self.__dict__[entry]) 
        return ret + "]"


# Fit file structure: Field Definition
class FieldDefinition:
    def __init__(self, mesgTypeDef, f):
        self.fieldDefNumber = ord(f.read(1))
        self.fieldName = getMessageDef(mesgTypeDef.valueName, self.fieldDefNumber).fieldName
        self.size = ord(f.read(1))
        byte = ord(f.read(1))
        self.endianAbility = (byte & 2 ** 7) != 0
        self.baseType = (byte & 31)

    def __repr__(self):
        return self.__class__.__name__ + "[" + str(self.fieldDefNumber) + "/" + self.fieldName + "]"
    
# Fit file structure: Field
class Field:
    def __init__(self, fieldDefinition, definitionMessage, messageName, f):
        self.fieldName = getMessageDef(messageName, fieldDefinition.fieldDefNumber).fieldName
        content = f.read(fieldDefinition.size)
        self.fieldDefNumber = fieldDefinition.fieldDefNumber
        self.value = convertFitBaseType(content, fieldDefinition.baseType, definitionMessage.architecture)
        
    def __repr__(self):
        return self.__class__.__name__ + "[" + self.fieldName + "/" + str(self.value) + "]"

# Fit file structure: DefinitionMessage
class DefinitionMessage:
    def __init__(self, f):
        f.read(1)  # reserved
        
        endianness = ord(f.read(1))
        if endianness == 0.:
            self.architecture = '<'  # this can be used by struct.unpack
        else: 
            self.architecture = '>'
            
        self.globalMsgNumber = struct.unpack(self.architecture + 'H', f.read(2))[0]
        self.mesgTypeDef = getTypeDef('mesg_num', self.globalMsgNumber)
        self.numFields = ord(f.read(1))
        self.fields = list()
        for i in xrange(self.numFields):
            self.fields.append(FieldDefinition(self.mesgTypeDef, f))
            
    def getDefMsgTitle(self, num):
        typeDef = getTypeDef('mesg_num', num)
        if typeDef <> None:
            return typeDef.valueName
        return "UNKNOWN"
            
    def __repr__(self):
        ret = self.__class__.__name__ + "[" + self.getDefMsgTitle(self.globalMsgNumber)
        for entry in self.__dict__:
            ret += ";" + entry + "=" + str(self.__dict__[entry]) 
        return ret + "]"
    
# Fit file structure: DataMessage
class DataMessage:
    def __init__(self, definitionMessage, f):
        self.messageName = definitionMessage.mesgTypeDef.valueName
        self.fields = list()
        for fieldDefinition in definitionMessage.fields:
            field = Field(fieldDefinition, definitionMessage, self.messageName, f)
            self.fields.append(field)
            
    def __repr__(self):
        ret = self.__class__.__name__ + "["
        for entry in self.__dict__:
            ret += ";" + entry + "=" 
            value = self.__dict__[entry]
            if type(value) is list:
                for member in value:
                    ret += ";" + str(member)
            else:
                ret += str(self.__dict__[entry]) 
        return ret + "]" 
    
# Helper class to read data    
class DataReader:
    def __init__(self, dataSize, f):
        self.dataSize = dataSize
        self.counter = 0
        self.f = f
        
    def read(self, length):
        if self.dataSize <= self.counter:
            return ''
        self.counter += length
        return self.f.read(length)
            

class FitReader: 

    def readActivity(self, fileName):
        f = open(fileName, 'rb')
        fileHeader = FileHeader(f)
        dr = DataReader(fileHeader.dataSize, f)
        definitionMessages = dict()
        dataMessages = dict()
    
        recordHeader = RecordHeader(dr)
        
        while not(recordHeader.endOfFile):
            if recordHeader.isDefinitionMessage:
                definitionMessage = DefinitionMessage(dr)
                definitionMessages[recordHeader.localMessageType] = definitionMessage
            
            if recordHeader.isDataMessage:
                definitionMessage = definitionMessages[recordHeader.localMessageType]
                dataMessage = DataMessage(definitionMessage, dr)
                if not (recordHeader.localMessageType in dataMessages):
                    dataMessages[recordHeader.localMessageType] = list()
                dataMessages[recordHeader.localMessageType].append(dataMessage)
                
            recordHeader = RecordHeader(dr)
            
        
        # simplify data structure: dict of list of dict
        # activity: key=defMessageTitle
        #           value=list of dict; key=fieldName
        #                               value=fieldValue
        activity = dict()
        for entry in dataMessages:
            dataMessageList = dataMessages[entry]
            for dataMessage in dataMessageList:
                messageName = dataMessage.messageName
                if not (messageName in activity):
                    activity[messageName] = list()
                valueDict = dict()
                for field in dataMessage.fields:
                    valueDict[field.fieldName] = field.value
                activity[messageName].append(valueDict)
        
        return activity


if __name__ == '__main__':
    
    fr = FitReader()
    activity = fr.readActivity('../../testdata/2014-02-18-16-13-41.fit')
    for entry in activity:
        print entry
        theList = activity[entry]  
        if len(theList) > 50:
            print len(theList) 
        else:
            print theList     
        
        
        
    
