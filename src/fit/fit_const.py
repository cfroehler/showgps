
'''
Created on 18.02.2014

@author: christian
'''

import struct


class MessageDefinition:
    def __init__(self, messageName, fieldDefId, fieldName, fieldType, scale, offset, units, comment):
        self.messageName = messageName
        self.fieldDefId = fieldDefId
        self.fieldName = fieldName
        self.fieldType = fieldType
        self.scale = scale
        self.offset = offset
        self.units = units
        self.comment = comment
        
    def __repr__(self):
        ret = self.__class__.__name__ + "["
        for entry in self.__dict__:
            ret += ";" + entry + "=" + str(self.__dict__[entry]) 
        return ret + "]"        
        
        
class TypeDefinition:
    def __init__(self, typeName, baseType, valueName, value, comment):
        self.typeName = typeName
        self.baseType = baseType
        self.valueName = valueName
        self.value = value
        self.comment = comment
        
    def __repr__(self):
        ret = self.__class__.__name__ + "["
        for entry in self.__dict__:
            ret += ";" + entry + "=" + str(self.__dict__[entry]) 
        return ret + "]"
            
messageDefDict = dict()
UNKNOWN_MESSAGE_DEF = MessageDefinition("UNKNOWN", -1, "UNKNOWN", "UNKNOWN", 0, 0, 0, "")

typeDefDict = dict()
UNKNOWN_TYPE_DEF = TypeDefinition("UNKNOWN", "UNKNOWN", "UNKNOWN", -1, "")

fitBaseType = dict((
             (0, 'B'),  # enum
             (1, 'b'),  # sint8
             (2, 'B'),  # uint8
             (3, 'h'),  # sint16
             (4, 'H'),  # uint16
             (5, 'i'),  # sint32
             (6, 'I'),  # uint32
             (7, 'c'),  # string
             (8, 'f'),  # float32
             (9, 'd'),  # float64
             (10,'B'),  # uint8z
             (11,'H'),  # uint16z
             (12,'I'),  # uint32z
             (13,'B')   # byte
             ))

def addMessageDef(messageName, fieldDefId, fieldName, fieldType, scale, offset, units, comment):
    md=MessageDefinition(messageName, fieldDefId, fieldName, fieldType, scale, offset, units, comment)
    if not(messageDefDict.has_key(messageName)):
        messageDefDict[messageName] = dict()
    idDict = messageDefDict[messageName]
    if not(idDict.has_key(fieldDefId)):
        idDict[fieldDefId] = dict()
    idDict[fieldDefId] = md
    
def getMessageDef(messageName, fieldDefId):
    if not(messageDefDict.has_key(messageName)):
        return UNKNOWN_MESSAGE_DEF
    if not(messageDefDict[messageName].has_key(fieldDefId)):
        return UNKNOWN_MESSAGE_DEF
    return messageDefDict[messageName][fieldDefId]
    

def addTypeDef(typeName, baseType, valueName, value, comment):
    td = TypeDefinition(typeName, baseType, valueName, value, comment)      
    if not(typeDefDict.has_key(typeName)):
        typeDefDict[typeName] = dict()
    typeDefDict[typeName][value]=td
    
def getTypeDef(typeName, value):
    if not(typeDefDict.has_key(typeName)):
        return UNKNOWN_TYPE_DEF
    if not(typeDefDict[typeName].has_key(value)):
        return UNKNOWN_TYPE_DEF
    return typeDefDict[typeName][value]


def convertFitBaseType(value, baseType, architecture):
    theType = fitBaseType[baseType]
    return struct.unpack(architecture + theType, value)[0]    


addTypeDef('file','enum','device',1,'Read only, single file. Must be in root directory.')
addTypeDef('file','enum','settings',2,'Read/write, single file. Directory=Settings')
addTypeDef('file','enum','sport',3,'Read/write, multiple files, file number = sport type. Directory=Sports')
addTypeDef('file','enum','activity',4,'Read/erase, multiple files. Directory=Activities')
addTypeDef('file','enum','workout',5,'Read/write/erase, multiple files. Directory=Workouts')
addTypeDef('file','enum','course',6,'Read/write/erase, multiple files. Directory=Courses')
addTypeDef('file','enum','schedules',7,'Read/write, single file. Directory=Schedules')
addTypeDef('file','enum','weight',9,'Read only, single file. Circular buffer. All message definitions at start of file. Directory=Weight')
addTypeDef('file','enum','totals',10,'Read only, single file. Directory=Totals')
addTypeDef('file','enum','goals',11,'Read/write, single file. Directory=Goals')
addTypeDef('file','enum','blood_pressure',14,'Read only. Directory=Blood Pressure')
addTypeDef('file','enum','monitoring_a',15,'Read only. Directory=Monitoring. File number=sub type.')
addTypeDef('file','enum','activity_summary',20,'Read/erase, multiple files. Directory=Activities')
addTypeDef('file','enum','monitoring_daily',28,'')
addTypeDef('file','enum','monitoring_b',32,'Read only. Directory=Monitoring. File number=identifier')


addTypeDef('mesg_num','uint16','file_id',0,'')
addTypeDef('mesg_num','uint16','capabilities',1,'')
addTypeDef('mesg_num','uint16','device_settings',2,'')
addTypeDef('mesg_num','uint16','user_profile',3,'')
addTypeDef('mesg_num','uint16','hrm_profile',4,'')
addTypeDef('mesg_num','uint16','sdm_profile',5,'')
addTypeDef('mesg_num','uint16','bike_profile',6,'')
addTypeDef('mesg_num','uint16','zones_target',7,'')
addTypeDef('mesg_num','uint16','hr_zone',8,'')
addTypeDef('mesg_num','uint16','power_zone',9,'')
addTypeDef('mesg_num','uint16','met_zone',10,'')
addTypeDef('mesg_num','uint16','sport',12,'')
addTypeDef('mesg_num','uint16','goal',15,'')
addTypeDef('mesg_num','uint16','session',18,'')
addTypeDef('mesg_num','uint16','lap',19,'')
addTypeDef('mesg_num','uint16','record',20,'')
addTypeDef('mesg_num','uint16','event',21,'')
addTypeDef('mesg_num','uint16','device_info',23,'')
addTypeDef('mesg_num','uint16','workout',26,'')
addTypeDef('mesg_num','uint16','workout_step',27,'')
addTypeDef('mesg_num','uint16','schedule',28,'')
addTypeDef('mesg_num','uint16','weight_scale',30,'')
addTypeDef('mesg_num','uint16','course',31,'')
addTypeDef('mesg_num','uint16','course_point',32,'')
addTypeDef('mesg_num','uint16','totals',33,'')
addTypeDef('mesg_num','uint16','activity',34,'')
addTypeDef('mesg_num','uint16','software',35,'')
addTypeDef('mesg_num','uint16','file_capabilities',37,'')
addTypeDef('mesg_num','uint16','mesg_capabilities',38,'')
addTypeDef('mesg_num','uint16','field_capabilities',39,'')
addTypeDef('mesg_num','uint16','file_creator',49,'')
addTypeDef('mesg_num','uint16','blood_pressure',51,'')
addTypeDef('mesg_num','uint16','speed_zone',53,'')
addTypeDef('mesg_num','uint16','monitoring',55,'')
addTypeDef('mesg_num','uint16','hrv',78,'')
addTypeDef('mesg_num','uint16','length',101,'')
addTypeDef('mesg_num','uint16','monitoring_info',103,'')
addTypeDef('mesg_num','uint16','pad',105,'')
addTypeDef('mesg_num','uint16','slave_device',106,'')
addTypeDef('mesg_num','uint16','cadence_zone',131,'')
addTypeDef('mesg_num','uint16','memo_glob',145,'')
addTypeDef('mesg_num','uint16','mfg_range_min',0xFF00,'0xFF00 - 0xFFFE reserved for manufacturer specific messages')
addTypeDef('mesg_num','uint16','mfg_range_max',0xFFFE,'0xFF00 - 0xFFFE reserved for manufacturer specific messages')


addTypeDef('checksum','uint8','clear',0,'Allows clear of checksum for flash memory where can only write 1 to 0 without erasing sector.')
addTypeDef('checksum','uint8','ok',1,'Set to mark checksum as valid if computes to invalid values 0 or 0xFF.  Checksum can also be set to ok to save encoding computation time.')


addTypeDef('file_flags','uint8z','read',0x02,'')
addTypeDef('file_flags','uint8z','write',0x04,'')
addTypeDef('file_flags','uint8z','erase',0x08,'')


addTypeDef('mesg_count','enum','num_per_file',0,'')
addTypeDef('mesg_count','enum','max_per_file',1,'')
addTypeDef('mesg_count','enum','max_per_file_type',2,'')


addTypeDef('date_time','uint32','min',0x10000000,'if date_time is < 0x10000000 then it is system time (seconds from device power on)')


addTypeDef('local_date_time','uint32','min',0x10000000,'if date_time is < 0x10000000 then it is system time (seconds from device power on)')


addTypeDef('message_index','uint16','selected',0x8000,'message is selected if set')
addTypeDef('message_index','uint16','reserved',0x7000,'reserved (default 0)')
addTypeDef('message_index','uint16','mask',0x0FFF,'index')


addTypeDef('device_index','uint8','creator',0,'Creator of the file is always device index 0.')


addTypeDef('gender','enum','female',0,'')
addTypeDef('gender','enum','male',1,'')


addTypeDef('language','enum','english',0,'')
addTypeDef('language','enum','french',1,'')
addTypeDef('language','enum','italian',2,'')
addTypeDef('language','enum','german',3,'')
addTypeDef('language','enum','spanish',4,'')
addTypeDef('language','enum','croatian',5,'')
addTypeDef('language','enum','czech',6,'')
addTypeDef('language','enum','danish',7,'')
addTypeDef('language','enum','dutch',8,'')
addTypeDef('language','enum','finnish',9,'')
addTypeDef('language','enum','greek',10,'')
addTypeDef('language','enum','hungarian',11,'')
addTypeDef('language','enum','norwegian',12,'')
addTypeDef('language','enum','polish',13,'')
addTypeDef('language','enum','portuguese',14,'')
addTypeDef('language','enum','slovakian',15,'')
addTypeDef('language','enum','slovenian',16,'')
addTypeDef('language','enum','swedish',17,'')
addTypeDef('language','enum','russian',18,'')
addTypeDef('language','enum','turkish',19,'')
addTypeDef('language','enum','latvian',20,'')
addTypeDef('language','enum','ukrainian',21,'')
addTypeDef('language','enum','arabic',22,'')
addTypeDef('language','enum','farsi',23,'')
addTypeDef('language','enum','bulgarian',24,'')
addTypeDef('language','enum','romanian',25,'')
addTypeDef('language','enum','custom',254,'')


addTypeDef('display_measure','enum','metric',0,'')
addTypeDef('display_measure','enum','statute',1,'')


addTypeDef('display_heart','enum','bpm',0,'')
addTypeDef('display_heart','enum','max',1,'')
addTypeDef('display_heart','enum','reserve',2,'')


addTypeDef('display_power','enum','watts',0,'')
addTypeDef('display_power','enum','percent_ftp',1,'')


addTypeDef('display_position','enum','degree',0,'dd.dddddd                       ')
addTypeDef('display_position','enum','degree_minute',1,'dddmm.mmm                       ')
addTypeDef('display_position','enum','degree_minute_second',2,'dddmmss                         ')
addTypeDef('display_position','enum','austrian_grid',3,'Austrian Grid (BMN)             ')
addTypeDef('display_position','enum','british_grid',4,'British National Grid           ')
addTypeDef('display_position','enum','dutch_grid',5,'Dutch grid system               ')
addTypeDef('display_position','enum','hungarian_grid',6,'Hungarian grid system           ')
addTypeDef('display_position','enum','finnish_grid',7,'Finnish grid system Zone3 KKJ27 ')
addTypeDef('display_position','enum','german_grid',8,'Gausss Krueger (German)         ')
addTypeDef('display_position','enum','icelandic_grid',9,'Icelandic Grid                  ')
addTypeDef('display_position','enum','indonesian_equatorial',10,'Indonesian Equatorial LCO       ')
addTypeDef('display_position','enum','indonesian_irian',11,'Indonesian Irian LCO            ')
addTypeDef('display_position','enum','indonesian_southern',12,'Indonesian Southern LCO         ')
addTypeDef('display_position','enum','india_zone_0',13,'India zone 0                    ')
addTypeDef('display_position','enum','india_zone_IA',14,'India zone IA                   ')
addTypeDef('display_position','enum','india_zone_IB',15,'India zone IB                   ')
addTypeDef('display_position','enum','india_zone_IIA',16,'India zone IIA                  ')
addTypeDef('display_position','enum','india_zone_IIB',17,'India zone IIB                  ')
addTypeDef('display_position','enum','india_zone_IIIA',18,'India zone IIIA                 ')
addTypeDef('display_position','enum','india_zone_IIIB',19,'India zone IIIB                 ')
addTypeDef('display_position','enum','india_zone_IVA',20,'India zone IVA                  ')
addTypeDef('display_position','enum','india_zone_IVB',21,'India zone IVB                  ')
addTypeDef('display_position','enum','irish_transverse',22,'Irish Transverse Mercator       ')
addTypeDef('display_position','enum','irish_grid',23,'Irish Grid                      ')
addTypeDef('display_position','enum','loran',24,'Loran TD                        ')
addTypeDef('display_position','enum','maidenhead_grid',25,'Maidenhead grid system          ')
addTypeDef('display_position','enum','mgrs_grid',26,'MGRS grid system                ')
addTypeDef('display_position','enum','new_zealand_grid',27,'New Zealand grid system         ')
addTypeDef('display_position','enum','new_zealand_transverse',28,'New Zealand Transverse Mercator ')
addTypeDef('display_position','enum','qatar_grid',29,'Qatar National Grid             ')
addTypeDef('display_position','enum','modified_swedish_grid',30,'Modified RT-90 (Sweden)         ')
addTypeDef('display_position','enum','swedish_grid',31,'RT-90 (Sweden)                  ')
addTypeDef('display_position','enum','south_african_grid',32,'South African Grid              ')
addTypeDef('display_position','enum','swiss_grid',33,'Swiss CH-1903 grid              ')
addTypeDef('display_position','enum','taiwan_grid',34,'Taiwan Grid                     ')
addTypeDef('display_position','enum','united_states_grid',35,'United States National Grid     ')
addTypeDef('display_position','enum','utm_ups_grid',36,'UTM/UPS grid system             ')
addTypeDef('display_position','enum','west_malayan',37,'West Malayan RSO                ')
addTypeDef('display_position','enum','borneo_rso',38,'Borneo RSO')
addTypeDef('display_position','enum','estonian_grid',39,'Estonian grid system')
addTypeDef('display_position','enum','latvian_grid',40,'Latvian Transverse Mercator')
addTypeDef('display_position','enum','swedish_ref_99_grid',41,'Reference Grid 99 TM (Swedish)')


addTypeDef('sport','enum','generic',0,'')
addTypeDef('sport','enum','running',1,'')
addTypeDef('sport','enum','cycling',2,'')
addTypeDef('sport','enum','transition',3,'Mulitsport transition')
addTypeDef('sport','enum','fitness_equipment',4,'')
addTypeDef('sport','enum','swimming',5,'')
addTypeDef('sport','enum','basketball',6,'')
addTypeDef('sport','enum','soccer',7,'')
addTypeDef('sport','enum','tennis',8,'')
addTypeDef('sport','enum','american_football',9,'')
addTypeDef('sport','enum','training',10,'')
addTypeDef('sport','enum','walking',11,'')
addTypeDef('sport','enum','cross_country_skiing',12,'')
addTypeDef('sport','enum','alpine_skiing',13,'')
addTypeDef('sport','enum','snowboarding',14,'')
addTypeDef('sport','enum','rowing',15,'')
addTypeDef('sport','enum','mountaineering',16,'')
addTypeDef('sport','enum','hiking',17,'')
addTypeDef('sport','enum','multisport',18,'')
addTypeDef('sport','enum','paddling',19,'')
addTypeDef('sport','enum','all',254,'All is for goals only to include all sports.')


addTypeDef('sport_bits_0','uint8z','generic',0x01,'')
addTypeDef('sport_bits_0','uint8z','running',0x02,'')
addTypeDef('sport_bits_0','uint8z','cycling',0x04,'')
addTypeDef('sport_bits_0','uint8z','transition',0x08,'Mulitsport transition')
addTypeDef('sport_bits_0','uint8z','fitness_equipment',0x10,'')
addTypeDef('sport_bits_0','uint8z','swimming',0x20,'')
addTypeDef('sport_bits_0','uint8z','basketball',0x40,'')
addTypeDef('sport_bits_0','uint8z','soccer',0x80,'')


addTypeDef('sport_bits_1','uint8z','tennis',0x01,'')
addTypeDef('sport_bits_1','uint8z','american_football',0x02,'')
addTypeDef('sport_bits_1','uint8z','training',0x04,'')
addTypeDef('sport_bits_1','uint8z','walking',0x08,'')
addTypeDef('sport_bits_1','uint8z','cross_country_skiing',0x10,'')
addTypeDef('sport_bits_1','uint8z','alpine_skiing',0x20,'')
addTypeDef('sport_bits_1','uint8z','snowboarding',0x40,'')
addTypeDef('sport_bits_1','uint8z','rowing',0x80,'')


addTypeDef('sport_bits_2','uint8z','mountaineering',0x01,'')
addTypeDef('sport_bits_2','uint8z','hiking',0x02,'')
addTypeDef('sport_bits_2','uint8z','multisport',0x04,'')
addTypeDef('sport_bits_2','uint8z','paddling',0x08,'')


addTypeDef('sub_sport','enum','generic',0,'')
addTypeDef('sub_sport','enum','treadmill',1,'Run/Fitness Equipment')
addTypeDef('sub_sport','enum','street',2,'Run')
addTypeDef('sub_sport','enum','trail',3,'Run')
addTypeDef('sub_sport','enum','track',4,'Run')
addTypeDef('sub_sport','enum','spin',5,'Cycling')
addTypeDef('sub_sport','enum','indoor_cycling',6,'Cycling/Fitness Equipment')
addTypeDef('sub_sport','enum','road',7,'Cycling')
addTypeDef('sub_sport','enum','mountain',8,'Cycling')
addTypeDef('sub_sport','enum','downhill',9,'Cycling')
addTypeDef('sub_sport','enum','recumbent',10,'Cycling')
addTypeDef('sub_sport','enum','cyclocross',11,'Cycling')
addTypeDef('sub_sport','enum','hand_cycling',12,'Cycling')
addTypeDef('sub_sport','enum','track_cycling',13,'Cycling')
addTypeDef('sub_sport','enum','indoor_rowing',14,'Fitness Equipment')
addTypeDef('sub_sport','enum','elliptical',15,'Fitness Equipment')
addTypeDef('sub_sport','enum','stair_climbing',16,'Fitness Equipment')
addTypeDef('sub_sport','enum','lap_swimming',17,'Swimming')
addTypeDef('sub_sport','enum','open_water',18,'Swimming')
addTypeDef('sub_sport','enum','flexibility_training',19,'Training')
addTypeDef('sub_sport','enum','strength_training',20,'Training')
addTypeDef('sub_sport','enum','warm_up',21,'Tennis')
addTypeDef('sub_sport','enum','match',22,'Tennis')
addTypeDef('sub_sport','enum','exercise',23,'Tennis')
addTypeDef('sub_sport','enum','challenge',24,'Tennis')
addTypeDef('sub_sport','enum','indoor_skiing',25,'Fitness Equipment')
addTypeDef('sub_sport','enum','cardio_training',26,'Training')
addTypeDef('sub_sport','enum','all',254,'')


addTypeDef('activity','enum','manual',0,'')
addTypeDef('activity','enum','auto_multi_sport',1,'')


addTypeDef('intensity','enum','active',0,'')
addTypeDef('intensity','enum','rest',1,'')
addTypeDef('intensity','enum','warmup',2,'')
addTypeDef('intensity','enum','cooldown',3,'')


addTypeDef('session_trigger','enum','activity_end',0,'')
addTypeDef('session_trigger','enum','manual',1,'User changed sport.')
addTypeDef('session_trigger','enum','auto_multi_sport',2,'Auto multi-sport feature is enabled and user pressed lap button to advance session.')
addTypeDef('session_trigger','enum','fitness_equipment',3,'Auto sport change caused by user linking to fitness equipment.')


addTypeDef('autolap_trigger','enum','time',0,'')
addTypeDef('autolap_trigger','enum','distance',1,'')
addTypeDef('autolap_trigger','enum','position_start',2,'')
addTypeDef('autolap_trigger','enum','position_lap',3,'')
addTypeDef('autolap_trigger','enum','position_waypoint',4,'')
addTypeDef('autolap_trigger','enum','position_marked',5,'')
addTypeDef('autolap_trigger','enum','off',6,'')


addTypeDef('lap_trigger','enum','manual',0,'')
addTypeDef('lap_trigger','enum','time',1,'')
addTypeDef('lap_trigger','enum','distance',2,'')
addTypeDef('lap_trigger','enum','position_start',3,'')
addTypeDef('lap_trigger','enum','position_lap',4,'')
addTypeDef('lap_trigger','enum','position_waypoint',5,'')
addTypeDef('lap_trigger','enum','position_marked',6,'')
addTypeDef('lap_trigger','enum','session_end',7,'')
addTypeDef('lap_trigger','enum','fitness_equipment',8,'')


addTypeDef('event','enum','timer',0,'Group 0.  Start / stop_all')
addTypeDef('event','enum','workout',3,'start / stop')
addTypeDef('event','enum','workout_step',4,'Start at beginning of workout.  Stop at end of each step.')
addTypeDef('event','enum','power_down',5,'stop_all group 0')
addTypeDef('event','enum','power_up',6,'stop_all group 0')
addTypeDef('event','enum','off_course',7,'start / stop group 0')
addTypeDef('event','enum','session',8,'Stop at end of each session.')
addTypeDef('event','enum','lap',9,'Stop at end of each lap.')
addTypeDef('event','enum','course_point',10,'marker')
addTypeDef('event','enum','battery',11,'marker')
addTypeDef('event','enum','virtual_partner_pace',12,'Group 1. Start at beginning of activity if VP enabled, when VP pace is changed during activity or VP enabled mid activity.  stop_disable when VP disabled.')
addTypeDef('event','enum','hr_high_alert',13,'Group 0.  Start / stop when in alert condition.')
addTypeDef('event','enum','hr_low_alert',14,'Group 0.  Start / stop when in alert condition.')
addTypeDef('event','enum','speed_high_alert',15,'Group 0.  Start / stop when in alert condition.')
addTypeDef('event','enum','speed_low_alert',16,'Group 0.  Start / stop when in alert condition.')
addTypeDef('event','enum','cad_high_alert',17,'Group 0.  Start / stop when in alert condition.')
addTypeDef('event','enum','cad_low_alert',18,'Group 0.  Start / stop when in alert condition.')
addTypeDef('event','enum','power_high_alert',19,'Group 0.  Start / stop when in alert condition.')
addTypeDef('event','enum','power_low_alert',20,'Group 0.  Start / stop when in alert condition.')
addTypeDef('event','enum','recovery_hr',21,'marker')
addTypeDef('event','enum','battery_low',22,'marker')
addTypeDef('event','enum','time_duration_alert',23,'Group 1.  Start if enabled mid activity (not required at start of activity). Stop when duration is reached.  stop_disable if disabled.')
addTypeDef('event','enum','distance_duration_alert',24,'Group 1.  Start if enabled mid activity (not required at start of activity). Stop when duration is reached.  stop_disable if disabled.')
addTypeDef('event','enum','calorie_duration_alert',25,'Group 1.  Start if enabled mid activity (not required at start of activity). Stop when duration is reached.  stop_disable if disabled.')
addTypeDef('event','enum','activity',26,'Group 1..  Stop at end of activity.')
addTypeDef('event','enum','fitness_equipment',27,'marker')
addTypeDef('event','enum','length',28,'Stop at end of each length.')
addTypeDef('event','enum','user_marker',32,'marker')
addTypeDef('event','enum','sport_point',33,'marker')
addTypeDef('event','enum','calibration',36,'start/stop/marker')


addTypeDef('event_type','enum','start',0,'')
addTypeDef('event_type','enum','stop',1,'')
addTypeDef('event_type','enum','consecutive_depreciated',2,'')
addTypeDef('event_type','enum','marker',3,'')
addTypeDef('event_type','enum','stop_all',4,'')
addTypeDef('event_type','enum','begin_depreciated',5,'')
addTypeDef('event_type','enum','end_depreciated',6,'')
addTypeDef('event_type','enum','end_all_depreciated',7,'')
addTypeDef('event_type','enum','stop_disable',8,'')
addTypeDef('event_type','enum','stop_disable_all',9,'')


addTypeDef('timer_trigger','enum','manual',0,'')
addTypeDef('timer_trigger','enum','auto',1,'')
addTypeDef('timer_trigger','enum','fitness_equipment',2,'')


addTypeDef('fitness_equipment_state','enum','ready',0,'')
addTypeDef('fitness_equipment_state','enum','in_use',1,'')
addTypeDef('fitness_equipment_state','enum','paused',2,'')
addTypeDef('fitness_equipment_state','enum','unknown',3,'lost connection to fitness equipment')


addTypeDef('activity_class','enum','level',0x7F,'0 to 100')
addTypeDef('activity_class','enum','level_max',100,'')
addTypeDef('activity_class','enum','athlete',0x80,'')


addTypeDef('hr_zone_calc','enum','custom',0,'')
addTypeDef('hr_zone_calc','enum','percent_max_hr',1,'')
addTypeDef('hr_zone_calc','enum','percent_hrr',2,'')


addTypeDef('pwr_zone_calc','enum','custom',0,'')
addTypeDef('pwr_zone_calc','enum','percent_ftp',1,'')


addTypeDef('wkt_step_duration','enum','time',0,'')
addTypeDef('wkt_step_duration','enum','distance',1,'')
addTypeDef('wkt_step_duration','enum','hr_less_than',2,'')
addTypeDef('wkt_step_duration','enum','hr_greater_than',3,'')
addTypeDef('wkt_step_duration','enum','calories',4,'')
addTypeDef('wkt_step_duration','enum','open',5,'')
addTypeDef('wkt_step_duration','enum','repeat_until_steps_cmplt',6,'')
addTypeDef('wkt_step_duration','enum','repeat_until_time',7,'')
addTypeDef('wkt_step_duration','enum','repeat_until_distance',8,'')
addTypeDef('wkt_step_duration','enum','repeat_until_calories',9,'')
addTypeDef('wkt_step_duration','enum','repeat_until_hr_less_than',10,'')
addTypeDef('wkt_step_duration','enum','repeat_until_hr_greater_than',11,'')
addTypeDef('wkt_step_duration','enum','repeat_until_power_less_than',12,'')
addTypeDef('wkt_step_duration','enum','repeat_until_power_greater_than',13,'')
addTypeDef('wkt_step_duration','enum','power_less_than',14,'')
addTypeDef('wkt_step_duration','enum','power_greater_than',15,'')


addTypeDef('wkt_step_target','enum','speed',0,'')
addTypeDef('wkt_step_target','enum','heart_rate',1,'')
addTypeDef('wkt_step_target','enum','open',2,'')
addTypeDef('wkt_step_target','enum','cadence',3,'')
addTypeDef('wkt_step_target','enum','power',4,'')
addTypeDef('wkt_step_target','enum','grade',5,'')
addTypeDef('wkt_step_target','enum','resistance',6,'')


addTypeDef('goal','enum','time',0,'')
addTypeDef('goal','enum','distance',1,'')
addTypeDef('goal','enum','calories',2,'')
addTypeDef('goal','enum','frequency',3,'')
addTypeDef('goal','enum','steps',4,'')


addTypeDef('goal_recurrence','enum','off',0,'')
addTypeDef('goal_recurrence','enum','daily',1,'')
addTypeDef('goal_recurrence','enum','weekly',2,'')
addTypeDef('goal_recurrence','enum','monthly',3,'')
addTypeDef('goal_recurrence','enum','yearly',4,'')
addTypeDef('goal_recurrence','enum','custom',5,'')


addTypeDef('schedule','enum','workout',0,'')
addTypeDef('schedule','enum','course',1,'')


addTypeDef('course_point','enum','generic',0,'')
addTypeDef('course_point','enum','summit',1,'')
addTypeDef('course_point','enum','valley',2,'')
addTypeDef('course_point','enum','water',3,'')
addTypeDef('course_point','enum','food',4,'')
addTypeDef('course_point','enum','danger',5,'')
addTypeDef('course_point','enum','left',6,'')
addTypeDef('course_point','enum','right',7,'')
addTypeDef('course_point','enum','straight',8,'')
addTypeDef('course_point','enum','first_aid',9,'')
addTypeDef('course_point','enum','fourth_category',10,'')
addTypeDef('course_point','enum','third_category',11,'')
addTypeDef('course_point','enum','second_category',12,'')
addTypeDef('course_point','enum','first_category',13,'')
addTypeDef('course_point','enum','hors_category',14,'')
addTypeDef('course_point','enum','sprint',15,'')
addTypeDef('course_point','enum','left_fork',16,'')
addTypeDef('course_point','enum','right_fork',17,'')
addTypeDef('course_point','enum','middle_fork',18,'')
addTypeDef('course_point','enum','slight_left',19,'')
addTypeDef('course_point','enum','sharp_left',20,'')
addTypeDef('course_point','enum','slight_right',21,'')
addTypeDef('course_point','enum','sharp_right',22,'')
addTypeDef('course_point','enum','u_turn',23,'')


addTypeDef('manufacturer','uint16','garmin',1,'')
addTypeDef('manufacturer','uint16','garmin_fr405_antfs',2,'Do not use.  Used by FR405 for ANTFS man id.')
addTypeDef('manufacturer','uint16','zephyr',3,'')
addTypeDef('manufacturer','uint16','dayton',4,'')
addTypeDef('manufacturer','uint16','idt',5,'')
addTypeDef('manufacturer','uint16','srm',6,'')
addTypeDef('manufacturer','uint16','quarq',7,'')
addTypeDef('manufacturer','uint16','ibike',8,'')
addTypeDef('manufacturer','uint16','saris',9,'')
addTypeDef('manufacturer','uint16','spark_hk',10,'')
addTypeDef('manufacturer','uint16','tanita',11,'')
addTypeDef('manufacturer','uint16','echowell',12,'')
addTypeDef('manufacturer','uint16','dynastream_oem',13,'')
addTypeDef('manufacturer','uint16','nautilus',14,'')
addTypeDef('manufacturer','uint16','dynastream',15,'')
addTypeDef('manufacturer','uint16','timex',16,'')
addTypeDef('manufacturer','uint16','metrigear',17,'')
addTypeDef('manufacturer','uint16','xelic',18,'')
addTypeDef('manufacturer','uint16','beurer',19,'')
addTypeDef('manufacturer','uint16','cardiosport',20,'')
addTypeDef('manufacturer','uint16','a_and_d',21,'')
addTypeDef('manufacturer','uint16','hmm',22,'')
addTypeDef('manufacturer','uint16','suunto',23,'')
addTypeDef('manufacturer','uint16','thita_elektronik',24,'')
addTypeDef('manufacturer','uint16','gpulse',25,'')
addTypeDef('manufacturer','uint16','clean_mobile',26,'')
addTypeDef('manufacturer','uint16','pedal_brain',27,'')
addTypeDef('manufacturer','uint16','peaksware',28,'')
addTypeDef('manufacturer','uint16','saxonar',29,'')
addTypeDef('manufacturer','uint16','lemond_fitness',30,'')
addTypeDef('manufacturer','uint16','dexcom',31,'')
addTypeDef('manufacturer','uint16','wahoo_fitness',32,'')
addTypeDef('manufacturer','uint16','octane_fitness',33,'')
addTypeDef('manufacturer','uint16','archinoetics',34,'')
addTypeDef('manufacturer','uint16','the_hurt_box',35,'')
addTypeDef('manufacturer','uint16','citizen_systems',36,'')
addTypeDef('manufacturer','uint16','magellan',37,'')
addTypeDef('manufacturer','uint16','osynce',38,'')
addTypeDef('manufacturer','uint16','holux',39,'')
addTypeDef('manufacturer','uint16','concept2',40,'')
addTypeDef('manufacturer','uint16','one_giant_leap',42,'')
addTypeDef('manufacturer','uint16','ace_sensor',43,'')
addTypeDef('manufacturer','uint16','brim_brothers',44,'')
addTypeDef('manufacturer','uint16','xplova',45,'')
addTypeDef('manufacturer','uint16','perception_digital',46,'')
addTypeDef('manufacturer','uint16','bf1systems',47,'')
addTypeDef('manufacturer','uint16','pioneer',48,'')
addTypeDef('manufacturer','uint16','spantec',49,'')
addTypeDef('manufacturer','uint16','metalogics',50,'')
addTypeDef('manufacturer','uint16','4iiiis',51,'')
addTypeDef('manufacturer','uint16','seiko_epson',52,'')
addTypeDef('manufacturer','uint16','seiko_epson_oem',53,'')
addTypeDef('manufacturer','uint16','ifor_powell',54,'')
addTypeDef('manufacturer','uint16','maxwell_guider',55,'')
addTypeDef('manufacturer','uint16','star_trac',56,'')
addTypeDef('manufacturer','uint16','breakaway',57,'')
addTypeDef('manufacturer','uint16','alatech_technology_ltd',58,'')
addTypeDef('manufacturer','uint16','mio_technology_europe',59,'')
addTypeDef('manufacturer','uint16','rotor',60,'')
addTypeDef('manufacturer','uint16','geonaute',61,'')
addTypeDef('manufacturer','uint16','id_bike',62,'')
addTypeDef('manufacturer','uint16','specialized',63,'')
addTypeDef('manufacturer','uint16','wtek',64,'')
addTypeDef('manufacturer','uint16','physical_enterprises',65,'')
addTypeDef('manufacturer','uint16','north_pole_engineering',66,'')
addTypeDef('manufacturer','uint16','bkool',67,'')
addTypeDef('manufacturer','uint16','cateye',68,'')
addTypeDef('manufacturer','uint16','stages_cycling',69,'')
addTypeDef('manufacturer','uint16','sigmasport',70,'')
addTypeDef('manufacturer','uint16','tomtom',71,'')
addTypeDef('manufacturer','uint16','peripedal',72,'')
addTypeDef('manufacturer','uint16','wattbike',73,'')
addTypeDef('manufacturer','uint16','moxy',76,'')
addTypeDef('manufacturer','uint16','ciclosport',77,'')
addTypeDef('manufacturer','uint16','powerbahn',78,'')
addTypeDef('manufacturer','uint16','acorn_projects_aps',79,'')
addTypeDef('manufacturer','uint16','lifebeam',80,'')
addTypeDef('manufacturer','uint16','bontrager',81,'')
addTypeDef('manufacturer','uint16','wellgo',82,'')
addTypeDef('manufacturer','uint16','development',255,'')
addTypeDef('manufacturer','uint16','actigraphcorp',5759,'')


addTypeDef('garmin_product','uint16','hrm1',1,'')
addTypeDef('garmin_product','uint16','axh01',2,'AXH01 HRM chipset')
addTypeDef('garmin_product','uint16','axb01',3,'')
addTypeDef('garmin_product','uint16','axb02',4,'')
addTypeDef('garmin_product','uint16','hrm2ss',5,'')
addTypeDef('garmin_product','uint16','dsi_alf02',6,'')
addTypeDef('garmin_product','uint16','hrm_run_single_byte_product_id',8,'hrm_run model for HRM ANT+ messaging')
addTypeDef('garmin_product','uint16','fr301_china',473,'')
addTypeDef('garmin_product','uint16','fr301_japan',474,'')
addTypeDef('garmin_product','uint16','fr301_korea',475,'')
addTypeDef('garmin_product','uint16','fr301_taiwan',494,'')
addTypeDef('garmin_product','uint16','fr405',717,'Forerunner 405')
addTypeDef('garmin_product','uint16','fr50',782,'Forerunner 50')
addTypeDef('garmin_product','uint16','fr405_japan',987,'')
addTypeDef('garmin_product','uint16','fr60',988,'Forerunner 60')
addTypeDef('garmin_product','uint16','dsi_alf01',1011,'')
addTypeDef('garmin_product','uint16','fr310xt',1018,'Forerunner 310')
addTypeDef('garmin_product','uint16','edge500',1036,'')
addTypeDef('garmin_product','uint16','fr110',1124,'Forerunner 110')
addTypeDef('garmin_product','uint16','edge800',1169,'')
addTypeDef('garmin_product','uint16','edge500_taiwan',1199,'')
addTypeDef('garmin_product','uint16','edge500_japan',1213,'')
addTypeDef('garmin_product','uint16','chirp',1253,'')
addTypeDef('garmin_product','uint16','fr110_japan',1274,'')
addTypeDef('garmin_product','uint16','edge200',1325,'')
addTypeDef('garmin_product','uint16','fr910xt',1328,'')
addTypeDef('garmin_product','uint16','edge800_taiwan',1333,'')
addTypeDef('garmin_product','uint16','edge800_japan',1334,'')
addTypeDef('garmin_product','uint16','alf04',1341,'')
addTypeDef('garmin_product','uint16','fr610',1345,'')
addTypeDef('garmin_product','uint16','fr210_japan',1360,'')
addTypeDef('garmin_product','uint16','vector_ss',1380,'')
addTypeDef('garmin_product','uint16','vector_cp',1381,'')
addTypeDef('garmin_product','uint16','edge800_china',1386,'')
addTypeDef('garmin_product','uint16','edge500_china',1387,'')
addTypeDef('garmin_product','uint16','fr610_japan',1410,'')
addTypeDef('garmin_product','uint16','edge500_korea',1422,'')
addTypeDef('garmin_product','uint16','fr70',1436,'')
addTypeDef('garmin_product','uint16','fr310xt_4t',1446,'')
addTypeDef('garmin_product','uint16','amx',1461,'')
addTypeDef('garmin_product','uint16','fr10',1482,'')
addTypeDef('garmin_product','uint16','edge800_korea',1497,'')
addTypeDef('garmin_product','uint16','swim',1499,'')
addTypeDef('garmin_product','uint16','fr910xt_china',1537,'')
addTypeDef('garmin_product','uint16','fenix',1551,'')
addTypeDef('garmin_product','uint16','edge200_taiwan',1555,'')
addTypeDef('garmin_product','uint16','edge510',1561,'')
addTypeDef('garmin_product','uint16','edge810',1567,'')
addTypeDef('garmin_product','uint16','tempe',1570,'')
addTypeDef('garmin_product','uint16','fr910xt_japan',1600,'')
addTypeDef('garmin_product','uint16','fr620',1623,'')
addTypeDef('garmin_product','uint16','fr220',1632,'')
addTypeDef('garmin_product','uint16','fr910xt_korea',1664,'')
addTypeDef('garmin_product','uint16','fr10_japan',1688,'')
addTypeDef('garmin_product','uint16','edge810_japan',1721,'')
addTypeDef('garmin_product','uint16','virb_elite',1735,'')
addTypeDef('garmin_product','uint16','edge_touring',1736,'Also Edge Touring Plus')
addTypeDef('garmin_product','uint16','edge510_japan',1742,'')
addTypeDef('garmin_product','uint16','hrm_run',1752,'')
addTypeDef('garmin_product','uint16','edge510_asia',1821,'')
addTypeDef('garmin_product','uint16','edge810_china',1822,'')
addTypeDef('garmin_product','uint16','edge810_taiwan',1823,'')
addTypeDef('garmin_product','uint16','vivo_fit',1837,'')
addTypeDef('garmin_product','uint16','virb_remote',1853,'')
addTypeDef('garmin_product','uint16','sdm4',10007,'SDM4 footpod')
addTypeDef('garmin_product','uint16','training_center',20119,'')
addTypeDef('garmin_product','uint16','android_antplus_plugin',65532,'')
addTypeDef('garmin_product','uint16','connect',65534,'Garmin Connect website')


addTypeDef('antplus_device_type','uint8','antfs',1,'')
addTypeDef('antplus_device_type','uint8','bike_power',11,'')
addTypeDef('antplus_device_type','uint8','environment_sensor_legacy',12,'')
addTypeDef('antplus_device_type','uint8','multi_sport_speed_distance',15,'')
addTypeDef('antplus_device_type','uint8','control',16,'')
addTypeDef('antplus_device_type','uint8','fitness_equipment',17,'')
addTypeDef('antplus_device_type','uint8','blood_pressure',18,'')
addTypeDef('antplus_device_type','uint8','geocache_node',19,'')
addTypeDef('antplus_device_type','uint8','light_electric_vehicle',20,'')
addTypeDef('antplus_device_type','uint8','env_sensor',25,'')
addTypeDef('antplus_device_type','uint8','racquet',26,'')
addTypeDef('antplus_device_type','uint8','weight_scale',119,'')
addTypeDef('antplus_device_type','uint8','heart_rate',120,'')
addTypeDef('antplus_device_type','uint8','bike_speed_cadence',121,'')
addTypeDef('antplus_device_type','uint8','bike_cadence',122,'')
addTypeDef('antplus_device_type','uint8','bike_speed',123,'')
addTypeDef('antplus_device_type','uint8','stride_speed_distance',124,'')


addTypeDef('ant_network','enum','public',0,'')
addTypeDef('ant_network','enum','antplus',1,'')
addTypeDef('ant_network','enum','antfs',2,'')
addTypeDef('ant_network','enum','private',3,'')


addTypeDef('workout_capabilities','uint32z','interval',0x00000001,'')
addTypeDef('workout_capabilities','uint32z','custom',0x00000002,'')
addTypeDef('workout_capabilities','uint32z','fitness_equipment',0x00000004,'')
addTypeDef('workout_capabilities','uint32z','firstbeat',0x00000008,'')
addTypeDef('workout_capabilities','uint32z','new_leaf',0x00000010,'')
addTypeDef('workout_capabilities','uint32z','tcx',0x00000020,'For backwards compatibility.  Watch should add missing id fields then clear flag.')
addTypeDef('workout_capabilities','uint32z','speed',0x00000080,'Speed source required for workout step.')
addTypeDef('workout_capabilities','uint32z','heart_rate',0x00000100,'Heart rate source required for workout step.')
addTypeDef('workout_capabilities','uint32z','distance',0x00000200,'Distance source required for workout step.')
addTypeDef('workout_capabilities','uint32z','cadence',0x00000400,'Cadence source required for workout step.')
addTypeDef('workout_capabilities','uint32z','power',0x00000800,'Power source required for workout step.')
addTypeDef('workout_capabilities','uint32z','grade',0x00001000,'Grade source required for workout step.')
addTypeDef('workout_capabilities','uint32z','resistance',0x00002000,'Resistance source required for workout step.')
addTypeDef('workout_capabilities','uint32z','protected',0x00004000,'')


addTypeDef('battery_status','uint8','new',1,'')
addTypeDef('battery_status','uint8','good',2,'')
addTypeDef('battery_status','uint8','ok',3,'')
addTypeDef('battery_status','uint8','low',4,'')
addTypeDef('battery_status','uint8','critical',5,'')


addTypeDef('hr_type','enum','normal',0,'')
addTypeDef('hr_type','enum','irregular',1,'')


addTypeDef('course_capabilities','uint32z','processed',0x00000001,'')
addTypeDef('course_capabilities','uint32z','valid',0x00000002,'')
addTypeDef('course_capabilities','uint32z','time',0x00000004,'')
addTypeDef('course_capabilities','uint32z','distance',0x00000008,'')
addTypeDef('course_capabilities','uint32z','position',0x00000010,'')
addTypeDef('course_capabilities','uint32z','heart_rate',0x00000020,'')
addTypeDef('course_capabilities','uint32z','power',0x00000040,'')
addTypeDef('course_capabilities','uint32z','cadence',0x00000080,'')
addTypeDef('course_capabilities','uint32z','training',0x00000100,'')
addTypeDef('course_capabilities','uint32z','navigation',0x00000200,'')


addTypeDef('weight','uint16','calculating',0xFFFE,'')


addTypeDef('workout_hr','uint32','bpm_offset',100,'')


addTypeDef('workout_power','uint32','watts_offset',1000,'')


addTypeDef('bp_status','enum','no_error',0,'')
addTypeDef('bp_status','enum','error_incomplete_data',1,'')
addTypeDef('bp_status','enum','error_no_measurement',2,'')
addTypeDef('bp_status','enum','error_data_out_of_range',3,'')
addTypeDef('bp_status','enum','error_irregular_heart_rate',4,'')


addTypeDef('user_local_id','uint16','local_min',0x0000,'')
addTypeDef('user_local_id','uint16','local_max',0x000F,'')
addTypeDef('user_local_id','uint16','stationary_min',0x0010,'')
addTypeDef('user_local_id','uint16','stationary_max',0x00FF,'')
addTypeDef('user_local_id','uint16','portable_min',0x0100,'')
addTypeDef('user_local_id','uint16','portable_max',0xFFFE,'')


addTypeDef('swim_stroke','enum','freestyle',0,'')
addTypeDef('swim_stroke','enum','backstroke',1,'')
addTypeDef('swim_stroke','enum','breaststroke',2,'')
addTypeDef('swim_stroke','enum','butterfly',3,'')
addTypeDef('swim_stroke','enum','drill',4,'')
addTypeDef('swim_stroke','enum','mixed',5,'')
addTypeDef('swim_stroke','enum','im',6,'IM is a mixed interval containing the same number of lengths for each of: Butterfly, Backstroke, Breaststroke, Freestyle, swam in that order.')


addTypeDef('activity_type','enum','generic',0,'')
addTypeDef('activity_type','enum','running',1,'')
addTypeDef('activity_type','enum','cycling',2,'')
addTypeDef('activity_type','enum','transition',3,'Mulitsport transition')
addTypeDef('activity_type','enum','fitness_equipment',4,'')
addTypeDef('activity_type','enum','swimming',5,'')
addTypeDef('activity_type','enum','walking',6,'')
addTypeDef('activity_type','enum','all',254,'All is for goals only to include all sports.')


addTypeDef('activity_subtype','enum','generic',0,'')
addTypeDef('activity_subtype','enum','treadmill',1,'Run')
addTypeDef('activity_subtype','enum','street',2,'Run')
addTypeDef('activity_subtype','enum','trail',3,'Run')
addTypeDef('activity_subtype','enum','track',4,'Run')
addTypeDef('activity_subtype','enum','spin',5,'Cycling')
addTypeDef('activity_subtype','enum','indoor_cycling',6,'Cycling')
addTypeDef('activity_subtype','enum','road',7,'Cycling')
addTypeDef('activity_subtype','enum','mountain',8,'Cycling')
addTypeDef('activity_subtype','enum','downhill',9,'Cycling')
addTypeDef('activity_subtype','enum','recumbent',10,'Cycling')
addTypeDef('activity_subtype','enum','cyclocross',11,'Cycling')
addTypeDef('activity_subtype','enum','hand_cycling',12,'Cycling')
addTypeDef('activity_subtype','enum','track_cycling',13,'Cycling')
addTypeDef('activity_subtype','enum','indoor_rowing',14,'Fitness Equipment')
addTypeDef('activity_subtype','enum','elliptical',15,'Fitness Equipment')
addTypeDef('activity_subtype','enum','stair_climbing',16,'Fitness Equipment')
addTypeDef('activity_subtype','enum','lap_swimming',17,'Swimming')
addTypeDef('activity_subtype','enum','open_water',18,'Swimming')
addTypeDef('activity_subtype','enum','all',254,'')


addTypeDef('activity_level','enum','low',0,'')
addTypeDef('activity_level','enum','medium',1,'')
addTypeDef('activity_level','enum','high',2,'')


addTypeDef('left_right_balance','uint8','mask',0x7F,'% contribution')
addTypeDef('left_right_balance','uint8','right',0x80,'data corresponds to right if set, otherwise unknown')


addTypeDef('left_right_balance_100','uint16','mask',0x3FFF,'% contribution scaled by 100')
addTypeDef('left_right_balance_100','uint16','right',0x8000,'data corresponds to right if set, otherwise unknown')


addTypeDef('length_type','enum','idle',0,'Rest period. Length with no strokes')
addTypeDef('length_type','enum','active',1,'Length with strokes.')


addTypeDef('connectivity_capabilities','uint32z','bluetooth',0x00000001,'')
addTypeDef('connectivity_capabilities','uint32z','bluetooth_le',0x00000002,'')
addTypeDef('connectivity_capabilities','uint32z','ant',0x00000004,'')
addTypeDef('connectivity_capabilities','uint32z','activity_upload',0x00000008,'')
addTypeDef('connectivity_capabilities','uint32z','course_download',0x00000010,'')
addTypeDef('connectivity_capabilities','uint32z','workout_download',0x00000020,'')
addTypeDef('connectivity_capabilities','uint32z','live_track',0x00000040,'')
addTypeDef('connectivity_capabilities','uint32z','weather_conditions',0x00000080,'')
addTypeDef('connectivity_capabilities','uint32z','weather_alerts',0x00000100,'')
addTypeDef('connectivity_capabilities','uint32z','gps_ephemeris_download',0x00000200,'')
addTypeDef('connectivity_capabilities','uint32z','explicit_archive',0x00000400,'')
addTypeDef('connectivity_capabilities','uint32z','setup_incomplete',0x00000800,'')


addTypeDef('stroke_type','enum','no_event',0,'')
addTypeDef('stroke_type','enum','other',1,'stroke was detected but cannot be identified')
addTypeDef('stroke_type','enum','serve',2,'')
addTypeDef('stroke_type','enum','forehand',3,'')
addTypeDef('stroke_type','enum','backhand',4,'')
addTypeDef('stroke_type','enum','smash',5,'')


addTypeDef('body_location','enum','left_leg',0,'')
addTypeDef('body_location','enum','left_calf',1,'')
addTypeDef('body_location','enum','left_shin',2,'')
addTypeDef('body_location','enum','left_hamstring',3,'')
addTypeDef('body_location','enum','left_quad',4,'')
addTypeDef('body_location','enum','left_glute',5,'')
addTypeDef('body_location','enum','right_leg',6,'')
addTypeDef('body_location','enum','right_calf',7,'')
addTypeDef('body_location','enum','right_shin',8,'')
addTypeDef('body_location','enum','right_hamstring',9,'')
addTypeDef('body_location','enum','right_quad',10,'')
addTypeDef('body_location','enum','right_glute',11,'')
addTypeDef('body_location','enum','torso_back',12,'')
addTypeDef('body_location','enum','left_lower_back',13,'')
addTypeDef('body_location','enum','left_upper_back',14,'')
addTypeDef('body_location','enum','right_lower_back',15,'')
addTypeDef('body_location','enum','right_upper_back',16,'')
addTypeDef('body_location','enum','torso_front',17,'')
addTypeDef('body_location','enum','left_abdomen',18,'')
addTypeDef('body_location','enum','left_chest',19,'')
addTypeDef('body_location','enum','right_abdomen',20,'')
addTypeDef('body_location','enum','right_chest',21,'')
addTypeDef('body_location','enum','left_arm',22,'')
addTypeDef('body_location','enum','left_shoulder',23,'')
addTypeDef('body_location','enum','left_bicep',24,'')
addTypeDef('body_location','enum','left_tricep',25,'')
addTypeDef('body_location','enum','left_brachioradialis',26,'Left anterior forearm')
addTypeDef('body_location','enum','left_forearm_extensors',27,'Left posterior forearm')
addTypeDef('body_location','enum','right_arm',28,'')
addTypeDef('body_location','enum','right_shoulder',29,'')
addTypeDef('body_location','enum','right_bicep',30,'')
addTypeDef('body_location','enum','right_tricep',31,'')
addTypeDef('body_location','enum','right_brachioradialis',32,'Right anterior forearm')
addTypeDef('body_location','enum','right_forearm_extensors',33,'Right posterior forearm')
addTypeDef('body_location','enum','neck',34,'')
addTypeDef('body_location','enum','throat',35,'')





addMessageDef('file_id',0,'type','file',1,0,'','')

addMessageDef('file_id',1,'manufacturer','manufacturer',1,0,'','')
addMessageDef('file_id',2,'product','uint16',1,0,'','')

addMessageDef('file_id',3,'serial_number','uint32z',1,0,'','')
addMessageDef('file_id',4,'time_created','date_time',1,0,'','Only set for files that are can be created/erased.')
addMessageDef('file_id',5,'number','uint16',1,0,'','Only set for files that are not created/erased.')


addMessageDef('file_creator',0,'software_version','uint16',1,0,'','')
addMessageDef('file_creator',1,'hardware_version','uint8',1,0,'','')



addMessageDef('software',254,'message_index','message_index',1,0,'','')
addMessageDef('software',3,'version','uint16',100,0,'','')
addMessageDef('software',5,'part_number','string',1,0,'','')


addMessageDef('slave_device',0,'manufacturer','manufacturer',1,0,'','')
addMessageDef('slave_device',1,'product','uint16',1,0,'','')



addMessageDef('capabilities',0,'languages','uint8z',1,0,'','Use language_bits_x types where x is index of array.')
addMessageDef('capabilities',1,'sports','sport_bits_0',1,0,'','Use sport_bits_x types where x is index of array.')
addMessageDef('capabilities',21,'workouts_supported','workout_capabilities',1,0,'','')
addMessageDef('capabilities',23,'connectivity_supported','connectivity_capabilities',1,0,'','')


addMessageDef('file_capabilities',254,'message_index','message_index',1,0,'','')
addMessageDef('file_capabilities',0,'type','file',1,0,'','')
addMessageDef('file_capabilities',1,'flags','file_flags',1,0,'','')
addMessageDef('file_capabilities',2,'directory','string',1,0,'','')
addMessageDef('file_capabilities',3,'max_count','uint16',1,0,'','')
addMessageDef('file_capabilities',4,'max_size','uint32',1,0,'bytes','')


addMessageDef('mesg_capabilities',254,'message_index','message_index',1,0,'','')
addMessageDef('mesg_capabilities',0,'file','file',1,0,'','')
addMessageDef('mesg_capabilities',1,'mesg_num','mesg_num',1,0,'','')
addMessageDef('mesg_capabilities',2,'count_type','mesg_count',1,0,'','')
addMessageDef('mesg_capabilities',3,'count','uint16',1,0,'','')





addMessageDef('field_capabilities',254,'message_index','message_index',1,0,'','')
addMessageDef('field_capabilities',0,'file','file',1,0,'','')
addMessageDef('field_capabilities',1,'mesg_num','mesg_num',1,0,'','')
addMessageDef('field_capabilities',2,'field_num','uint8',1,0,'','')
addMessageDef('field_capabilities',3,'count','uint16',1,0,'','')



addMessageDef('device_settings',1,'utc_offset','uint32',1,0,'','Offset from system time. Required to convert timestamp from system time to UTC.')


addMessageDef('user_profile',254,'message_index','message_index',1,0,'','')
addMessageDef('user_profile',0,'friendly_name','string',1,0,'','')
addMessageDef('user_profile',1,'gender','gender',1,0,'','')
addMessageDef('user_profile',2,'age','uint8',1,0,'years','')
addMessageDef('user_profile',3,'height','uint8',100,0,'m','')
addMessageDef('user_profile',4,'weight','uint16',10,0,'kg','')
addMessageDef('user_profile',5,'language','language',1,0,'','')
addMessageDef('user_profile',6,'elev_setting','display_measure',1,0,'','')
addMessageDef('user_profile',7,'weight_setting','display_measure',1,0,'','')
addMessageDef('user_profile',8,'resting_heart_rate','uint8',1,0,'bpm','')
addMessageDef('user_profile',9,'default_max_running_heart_rate','uint8',1,0,'bpm','')
addMessageDef('user_profile',10,'default_max_biking_heart_rate','uint8',1,0,'bpm','')
addMessageDef('user_profile',11,'default_max_heart_rate','uint8',1,0,'bpm','')
addMessageDef('user_profile',12,'hr_setting','display_heart',1,0,'','')
addMessageDef('user_profile',13,'speed_setting','display_measure',1,0,'','')
addMessageDef('user_profile',14,'dist_setting','display_measure',1,0,'','')
addMessageDef('user_profile',16,'power_setting','display_power',1,0,'','')
addMessageDef('user_profile',17,'activity_class','activity_class',1,0,'','')
addMessageDef('user_profile',18,'position_setting','display_position',1,0,'','')
addMessageDef('user_profile',21,'temperature_setting','display_measure',1,0,'','')
addMessageDef('user_profile',22,'local_id','user_local_id',1,0,'','')
addMessageDef('user_profile',23,'global_id','byte',1,0,'','')
addMessageDef('user_profile',30,'height_setting','display_measure',1,0,'','')


addMessageDef('hrm_profile',254,'message_index','message_index',1,0,'','')
addMessageDef('hrm_profile',0,'enabled','bool',1,0,'','')
addMessageDef('hrm_profile',1,'hrm_ant_id','uint16z',1,0,'','')
addMessageDef('hrm_profile',2,'log_hrv','bool',1,0,'','')
addMessageDef('hrm_profile',3,'hrm_ant_id_trans_type','uint8z',1,0,'','')


addMessageDef('sdm_profile',254,'message_index','message_index',1,0,'','')
addMessageDef('sdm_profile',0,'enabled','bool',1,0,'','')
addMessageDef('sdm_profile',1,'sdm_ant_id','uint16z',1,0,'','')
addMessageDef('sdm_profile',2,'sdm_cal_factor','uint16',10,0,'%','')
addMessageDef('sdm_profile',3,'odometer','uint32',100,0,'m','')
addMessageDef('sdm_profile',4,'speed_source','bool',1,0,'','Use footpod for speed source instead of GPS')
addMessageDef('sdm_profile',5,'sdm_ant_id_trans_type','uint8z',1,0,'','')
addMessageDef('sdm_profile',7,'odometer_rollover','uint8',1,0,'','Rollover counter that can be used to extend the odometer')


addMessageDef('bike_profile',254,'message_index','message_index',1,0,'','')
addMessageDef('bike_profile',0,'name','string',1,0,'','')
addMessageDef('bike_profile',1,'sport','sport',1,0,'','')
addMessageDef('bike_profile',2,'sub_sport','sub_sport',1,0,'','')
addMessageDef('bike_profile',3,'odometer','uint32',100,0,'m','')
addMessageDef('bike_profile',4,'bike_spd_ant_id','uint16z',1,0,'','')
addMessageDef('bike_profile',5,'bike_cad_ant_id','uint16z',1,0,'','')
addMessageDef('bike_profile',6,'bike_spdcad_ant_id','uint16z',1,0,'','')
addMessageDef('bike_profile',7,'bike_power_ant_id','uint16z',1,0,'','')
addMessageDef('bike_profile',8,'custom_wheelsize','uint16',1000,0,'m','')
addMessageDef('bike_profile',9,'auto_wheelsize','uint16',1000,0,'m','')
addMessageDef('bike_profile',10,'bike_weight','uint16',10,0,'kg','')
addMessageDef('bike_profile',11,'power_cal_factor','uint16',10,0,'%','')
addMessageDef('bike_profile',12,'auto_wheel_cal','bool',1,0,'','')
addMessageDef('bike_profile',13,'auto_power_zero','bool',1,0,'','')
addMessageDef('bike_profile',14,'id','uint8',1,0,'','')
addMessageDef('bike_profile',15,'spd_enabled','bool',1,0,'','')
addMessageDef('bike_profile',16,'cad_enabled','bool',1,0,'','')
addMessageDef('bike_profile',17,'spdcad_enabled','bool',1,0,'','')
addMessageDef('bike_profile',18,'power_enabled','bool',1,0,'','')
addMessageDef('bike_profile',19,'crank_length','uint8',2,-110,'mm','')
addMessageDef('bike_profile',20,'enabled','bool',1,0,'','')
addMessageDef('bike_profile',21,'bike_spd_ant_id_trans_type','uint8z',1,0,'','')
addMessageDef('bike_profile',22,'bike_cad_ant_id_trans_type','uint8z',1,0,'','')
addMessageDef('bike_profile',23,'bike_spdcad_ant_id_trans_type','uint8z',1,0,'','')
addMessageDef('bike_profile',24,'bike_power_ant_id_trans_type','uint8z',1,0,'','')
addMessageDef('bike_profile',37,'odometer_rollover','uint8',1,0,'','Rollover counter that can be used to extend the odometer')



addMessageDef('zones_target',1,'max_heart_rate','uint8',1,0,'','')
addMessageDef('zones_target',2,'threshold_heart_rate','uint8',1,0,'','')
addMessageDef('zones_target',3,'functional_threshold_power','uint16',1,0,'','')
addMessageDef('zones_target',5,'hr_calc_type','hr_zone_calc',1,0,'','')
addMessageDef('zones_target',7,'pwr_calc_type','pwr_zone_calc',1,0,'','')


addMessageDef('sport',0,'sport','sport',1,0,'','')
addMessageDef('sport',1,'sub_sport','sub_sport',1,0,'','')
addMessageDef('sport',3,'name','string',1,0,'','')


addMessageDef('hr_zone',254,'message_index','message_index',1,0,'','')
addMessageDef('hr_zone',1,'high_bpm','uint8',1,0,'bpm','')
addMessageDef('hr_zone',2,'name','string',1,0,'','')


addMessageDef('speed_zone',254,'message_index','message_index',1,0,'','')
addMessageDef('speed_zone',0,'high_value','uint16',1000,0,'m/s','')
addMessageDef('speed_zone',1,'name','string',1,0,'','')


addMessageDef('cadence_zone',254,'message_index','message_index',1,0,'','')
addMessageDef('cadence_zone',0,'high_value','uint8',1,0,'rpm','')
addMessageDef('cadence_zone',1,'name','string',1,0,'','')


addMessageDef('power_zone',254,'message_index','message_index',1,0,'','')
addMessageDef('power_zone',1,'high_value','uint16',1,0,'watts','')
addMessageDef('power_zone',2,'name','string',1,0,'','')


addMessageDef('met_zone',254,'message_index','message_index',1,0,'','')
addMessageDef('met_zone',1,'high_bpm','uint8',1,0,'','')
addMessageDef('met_zone',2,'calories','uint16',10,0,'kcal / min','')
addMessageDef('met_zone',3,'fat_calories','uint8',10,0,'kcal / min','')



addMessageDef('goal',254,'message_index','message_index',1,0,'','')
addMessageDef('goal',0,'sport','sport',1,0,'','')
addMessageDef('goal',1,'sub_sport','sub_sport',1,0,'','')
addMessageDef('goal',2,'start_date','date_time',1,0,'','')
addMessageDef('goal',3,'end_date','date_time',1,0,'','')
addMessageDef('goal',4,'type','goal',1,0,'','')
addMessageDef('goal',5,'value','uint32',1,0,'','')
addMessageDef('goal',6,'repeat','bool',1,0,'','')
addMessageDef('goal',7,'target_value','uint32',1,0,'','')
addMessageDef('goal',8,'recurrence','goal_recurrence',1,0,'','')
addMessageDef('goal',9,'recurrence_value','uint16',1,0,'','')
addMessageDef('goal',10,'enabled','bool',1,0,'','')



addMessageDef('activity',253,'timestamp','date_time',1,0,'','')
addMessageDef('activity',0,'total_timer_time','uint32',1000,0,'s','Exclude pauses')
addMessageDef('activity',1,'num_sessions','uint16',1,0,'','')
addMessageDef('activity',2,'type','activity',1,0,'','')
addMessageDef('activity',3,'event','event',1,0,'','')
addMessageDef('activity',4,'event_type','event_type',1,0,'','')
addMessageDef('activity',5,'local_timestamp','local_date_time',1,0,'','timestamp epoch expressed in local time, used to convert activity timestamps to local time ')
addMessageDef('activity',6,'event_group','uint8',1,0,'','')


addMessageDef('session',254,'message_index','message_index',1,0,'','Selected bit is set for the current session.')
addMessageDef('session',253,'timestamp','date_time',1,0,'s','Sesson end time.')
addMessageDef('session',0,'event','event',1,0,'','session')
addMessageDef('session',1,'event_type','event_type',1,0,'','stop')
addMessageDef('session',2,'start_time','date_time',1,0,'','')
addMessageDef('session',3,'start_position_lat','sint32',1,0,'semicircles','')
addMessageDef('session',4,'start_position_long','sint32',1,0,'semicircles','')
addMessageDef('session',5,'sport','sport',1,0,'','')
addMessageDef('session',6,'sub_sport','sub_sport',1,0,'','')
addMessageDef('session',7,'total_elapsed_time','uint32',1000,0,'s','Time (includes pauses)')
addMessageDef('session',8,'total_timer_time','uint32',1000,0,'s','Timer Time (excludes pauses)')
addMessageDef('session',9,'total_distance','uint32',100,0,'m','')
addMessageDef('session',10,'total_cycles','uint32',1,0,'cycles','')

addMessageDef('session',11,'total_calories','uint16',1,0,'kcal','')
addMessageDef('session',13,'total_fat_calories','uint16',1,0,'kcal','')
addMessageDef('session',14,'avg_speed','uint16',1000,0,'m/s','total_distance / total_timer_time')
addMessageDef('session',15,'max_speed','uint16',1000,0,'m/s','')
addMessageDef('session',16,'avg_heart_rate','uint8',1,0,'bpm','average heart rate (excludes pause time)')
addMessageDef('session',17,'max_heart_rate','uint8',1,0,'bpm','')
addMessageDef('session',18,'avg_cadence','uint8',1,0,'rpm','total_cycles / total_timer_time if non_zero_avg_cadence otherwise total_cycles / total_elapsed_time')

addMessageDef('session',19,'max_cadence','uint8',1,0,'rpm','')

addMessageDef('session',20,'avg_power','uint16',1,0,'watts','total_power / total_timer_time if non_zero_avg_power otherwise total_power / total_elapsed_time')
addMessageDef('session',21,'max_power','uint16',1,0,'watts','')
addMessageDef('session',22,'total_ascent','uint16',1,0,'m','')
addMessageDef('session',23,'total_descent','uint16',1,0,'m','')
addMessageDef('session',24,'total_training_effect','uint8',10,0,'','')
addMessageDef('session',25,'first_lap_index','uint16',1,0,'','')
addMessageDef('session',26,'num_laps','uint16',1,0,'','')
addMessageDef('session',27,'event_group','uint8',1,0,'','')
addMessageDef('session',28,'trigger','session_trigger',1,0,'','')
addMessageDef('session',29,'nec_lat','sint32',1,0,'semicircles','')
addMessageDef('session',30,'nec_long','sint32',1,0,'semicircles','')
addMessageDef('session',31,'swc_lat','sint32',1,0,'semicircles','')
addMessageDef('session',32,'swc_long','sint32',1,0,'semicircles','')
addMessageDef('session',34,'normalized_power','uint16',1,0,'watts','')
addMessageDef('session',35,'training_stress_score','uint16',10,0,'tss','')
addMessageDef('session',36,'intensity_factor','uint16',1000,0,'if','')
addMessageDef('session',37,'left_right_balance','left_right_balance_100',1,0,'','')
addMessageDef('session',41,'avg_stroke_count','uint32',10,0,'strokes/lap','')
addMessageDef('session',42,'avg_stroke_distance','uint16',100,0,'m','')
addMessageDef('session',43,'swim_stroke','swim_stroke',1,0,'swim_stroke','')
addMessageDef('session',44,'pool_length','uint16',100,0,'m','')
addMessageDef('session',46,'pool_length_unit','display_measure',1,0,'','')
addMessageDef('session',47,'num_active_lengths','uint16',1,0,'lengths','# of active lengths of swim pool')
addMessageDef('session',48,'total_work','uint32',1,0,'J','')
addMessageDef('session',49,'avg_altitude','uint16',5,500,'m','')
addMessageDef('session',50,'max_altitude','uint16',5,500,'m','')
addMessageDef('session',51,'gps_accuracy','uint8',1,0,'m','')
addMessageDef('session',52,'avg_grade','sint16',100,0,'%','')
addMessageDef('session',53,'avg_pos_grade','sint16',100,0,'%','')
addMessageDef('session',54,'avg_neg_grade','sint16',100,0,'%','')
addMessageDef('session',55,'max_pos_grade','sint16',100,0,'%','')
addMessageDef('session',56,'max_neg_grade','sint16',100,0,'%','')
addMessageDef('session',57,'avg_temperature','sint8',1,0,'C','')
addMessageDef('session',58,'max_temperature','sint8',1,0,'C','')
addMessageDef('session',59,'total_moving_time','uint32',1000,0,'s','')
addMessageDef('session',60,'avg_pos_vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('session',61,'avg_neg_vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('session',62,'max_pos_vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('session',63,'max_neg_vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('session',64,'min_heart_rate','uint8',1,0,'bpm','')
addMessageDef('session',65,'time_in_hr_zone','uint32',1000,0,'s','')
addMessageDef('session',66,'time_in_speed_zone','uint32',1000,0,'s','')
addMessageDef('session',67,'time_in_cadence_zone','uint32',1000,0,'s','')
addMessageDef('session',68,'time_in_power_zone','uint32',1000,0,'s','')
addMessageDef('session',69,'avg_lap_time','uint32',1000,0,'s','')
addMessageDef('session',70,'best_lap_index','uint16',1,0,'','')
addMessageDef('session',71,'min_altitude','uint16',5,500,'m','')
addMessageDef('session',82,'player_score','uint16',1,0,'','')
addMessageDef('session',83,'opponent_score','uint16',1,0,'','')
addMessageDef('session',84,'opponent_name','string',1,0,'','')
addMessageDef('session',85,'stroke_count','uint16',1,0,'counts','stroke_type enum used as the index')
addMessageDef('session',86,'zone_count','uint16',1,0,'counts','zone number used as the index')
addMessageDef('session',87,'max_ball_speed','uint16',100,0,'m/s','')
addMessageDef('session',88,'avg_ball_speed','uint16',100,0,'m/s','')
addMessageDef('session',89,'avg_vertical_oscillation','uint16',10,0,'mm','')
addMessageDef('session',90,'avg_stance_time_percent','uint16',100,0,'percent','')
addMessageDef('session',91,'avg_stance_time','uint16',10,0,'ms','')
addMessageDef('session',92,'avg_fractional_cadence','uint8',128,0,'rpm','fractional part of the avg_cadence')
addMessageDef('session',93,'max_fractional_cadence','uint8',128,0,'rpm','fractional part of the max_cadence')
addMessageDef('session',94,'total_fractional_cycles','uint8',128,0,'cycles','fractional part of the total_cycles')
addMessageDef('session',95,'avg_total_hemoglobin_conc','uint16',100,0,'g/dL','Avg saturated and unsaturated hemoglobin')
addMessageDef('session',96,'min_total_hemoglobin_conc','uint16',100,0,'g/dL','Min saturated and unsaturated hemoglobin')
addMessageDef('session',97,'max_total_hemoglobin_conc','uint16',100,0,'g/dL','Max saturated and unsaturated hemoglobin')
addMessageDef('session',98,'avg_saturated_hemoglobin_percent','uint16',10,0,'%','Avg percentage of hemoglobin saturated with oxygen')
addMessageDef('session',99,'min_saturated_hemoglobin_percent','uint16',10,0,'%','Min percentage of hemoglobin saturated with oxygen')
addMessageDef('session',100,'max_saturated_hemoglobin_percent','uint16',10,0,'%','Max percentage of hemoglobin saturated with oxygen')
addMessageDef('session',101,'avg_left_torque_effectiveness','uint8',2,0,'percent','')
addMessageDef('session',102,'avg_right_torque_effectiveness','uint8',2,0,'percent','')
addMessageDef('session',103,'avg_left_pedal_smoothness','uint8',2,0,'percent','')
addMessageDef('session',104,'avg_right_pedal_smoothness','uint8',2,0,'percent','')
addMessageDef('session',105,'avg_combined_pedal_smoothness','uint8',2,0,'percent','')


addMessageDef('lap',254,'message_index','message_index',1,0,'','')
addMessageDef('lap',253,'timestamp','date_time',1,0,'s','Lap end time.')
addMessageDef('lap',0,'event','event',1,0,'','')
addMessageDef('lap',1,'event_type','event_type',1,0,'','')
addMessageDef('lap',2,'start_time','date_time',1,0,'','')
addMessageDef('lap',3,'start_position_lat','sint32',1,0,'semicircles','')
addMessageDef('lap',4,'start_position_long','sint32',1,0,'semicircles','')
addMessageDef('lap',5,'end_position_lat','sint32',1,0,'semicircles','')
addMessageDef('lap',6,'end_position_long','sint32',1,0,'semicircles','')
addMessageDef('lap',7,'total_elapsed_time','uint32',1000,0,'s','Time (includes pauses)')
addMessageDef('lap',8,'total_timer_time','uint32',1000,0,'s','Timer Time (excludes pauses)')
addMessageDef('lap',9,'total_distance','uint32',100,0,'m','')
addMessageDef('lap',10,'total_cycles','uint32',1,0,'cycles','')

addMessageDef('lap',11,'total_calories','uint16',1,0,'kcal','')
addMessageDef('lap',12,'total_fat_calories','uint16',1,0,'kcal','If New Leaf')
addMessageDef('lap',13,'avg_speed','uint16',1000,0,'m/s','')
addMessageDef('lap',14,'max_speed','uint16',1000,0,'m/s','')
addMessageDef('lap',15,'avg_heart_rate','uint8',1,0,'bpm','')
addMessageDef('lap',16,'max_heart_rate','uint8',1,0,'bpm','')
addMessageDef('lap',17,'avg_cadence','uint8',1,0,'rpm','total_cycles / total_timer_time if non_zero_avg_cadence otherwise total_cycles / total_elapsed_time')

addMessageDef('lap',18,'max_cadence','uint8',1,0,'rpm','')

addMessageDef('lap',19,'avg_power','uint16',1,0,'watts','total_power / total_timer_time if non_zero_avg_power otherwise total_power / total_elapsed_time')
addMessageDef('lap',20,'max_power','uint16',1,0,'watts','')
addMessageDef('lap',21,'total_ascent','uint16',1,0,'m','')
addMessageDef('lap',22,'total_descent','uint16',1,0,'m','')
addMessageDef('lap',23,'intensity','intensity',1,0,'','')
addMessageDef('lap',24,'lap_trigger','lap_trigger',1,0,'','')
addMessageDef('lap',25,'sport','sport',1,0,'','')
addMessageDef('lap',26,'event_group','uint8',1,0,'','')
addMessageDef('lap',32,'num_lengths','uint16',1,0,'lengths','# of lengths of swim pool')
addMessageDef('lap',33,'normalized_power','uint16',1,0,'watts','')
addMessageDef('lap',34,'left_right_balance','left_right_balance_100',1,0,'','')
addMessageDef('lap',35,'first_length_index','uint16',1,0,'','')
addMessageDef('lap',37,'avg_stroke_distance','uint16',100,0,'m','')
addMessageDef('lap',38,'swim_stroke','swim_stroke',1,0,'','')
addMessageDef('lap',39,'sub_sport','sub_sport',1,0,'','')
addMessageDef('lap',40,'num_active_lengths','uint16',1,0,'lengths','# of active lengths of swim pool')
addMessageDef('lap',41,'total_work','uint32',1,0,'J','')
addMessageDef('lap',42,'avg_altitude','uint16',5,500,'m','')
addMessageDef('lap',43,'max_altitude','uint16',5,500,'m','')
addMessageDef('lap',44,'gps_accuracy','uint8',1,0,'m','')
addMessageDef('lap',45,'avg_grade','sint16',100,0,'%','')
addMessageDef('lap',46,'avg_pos_grade','sint16',100,0,'%','')
addMessageDef('lap',47,'avg_neg_grade','sint16',100,0,'%','')
addMessageDef('lap',48,'max_pos_grade','sint16',100,0,'%','')
addMessageDef('lap',49,'max_neg_grade','sint16',100,0,'%','')
addMessageDef('lap',50,'avg_temperature','sint8',1,0,'C','')
addMessageDef('lap',51,'max_temperature','sint8',1,0,'C','')
addMessageDef('lap',52,'total_moving_time','uint32',1000,0,'s','')
addMessageDef('lap',53,'avg_pos_vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('lap',54,'avg_neg_vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('lap',55,'max_pos_vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('lap',56,'max_neg_vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('lap',57,'time_in_hr_zone','uint32',1000,0,'s','')
addMessageDef('lap',58,'time_in_speed_zone','uint32',1000,0,'s','')
addMessageDef('lap',59,'time_in_cadence_zone','uint32',1000,0,'s','')
addMessageDef('lap',60,'time_in_power_zone','uint32',1000,0,'s','')
addMessageDef('lap',61,'repetition_num','uint16',1,0,'','')
addMessageDef('lap',62,'min_altitude','uint16',5,500,'m','')
addMessageDef('lap',63,'min_heart_rate','uint8',1,0,'bpm','')
addMessageDef('lap',71,'wkt_step_index','message_index',1,0,'','')
addMessageDef('lap',74,'opponent_score','uint16',1,0,'','')
addMessageDef('lap',75,'stroke_count','uint16',1,0,'counts','stroke_type enum used as the index')
addMessageDef('lap',76,'zone_count','uint16',1,0,'counts','zone number used as the index')
addMessageDef('lap',77,'avg_vertical_oscillation','uint16',10,0,'mm','')
addMessageDef('lap',78,'avg_stance_time_percent','uint16',100,0,'percent','')
addMessageDef('lap',79,'avg_stance_time','uint16',10,0,'ms','')
addMessageDef('lap',80,'avg_fractional_cadence','uint8',128,0,'rpm','fractional part of the avg_cadence')
addMessageDef('lap',81,'max_fractional_cadence','uint8',128,0,'rpm','fractional part of the max_cadence')
addMessageDef('lap',82,'total_fractional_cycles','uint8',128,0,'cycles','fractional part of the total_cycles')
addMessageDef('lap',83,'player_score','uint16',1,0,'','')
addMessageDef('lap',84,'avg_total_hemoglobin_conc','uint16',100,0,'g/dL','Avg saturated and unsaturated hemoglobin')
addMessageDef('lap',85,'min_total_hemoglobin_conc','uint16',100,0,'g/dL','Min saturated and unsaturated hemoglobin')
addMessageDef('lap',86,'max_total_hemoglobin_conc','uint16',100,0,'g/dL','Max saturated and unsaturated hemoglobin')
addMessageDef('lap',87,'avg_saturated_hemoglobin_percent','uint16',10,0,'%','Avg percentage of hemoglobin saturated with oxygen')
addMessageDef('lap',88,'min_saturated_hemoglobin_percent','uint16',10,0,'%','Min percentage of hemoglobin saturated with oxygen')
addMessageDef('lap',89,'max_saturated_hemoglobin_percent','uint16',10,0,'%','Max percentage of hemoglobin saturated with oxygen')
addMessageDef('lap',91,'avg_left_torque_effectiveness','uint8',2,0,'percent','')
addMessageDef('lap',92,'avg_right_torque_effectiveness','uint8',2,0,'percent','')
addMessageDef('lap',93,'avg_left_pedal_smoothness','uint8',2,0,'percent','')
addMessageDef('lap',94,'avg_right_pedal_smoothness','uint8',2,0,'percent','')
addMessageDef('lap',95,'avg_combined_pedal_smoothness','uint8',2,0,'percent','')


addMessageDef('length',254,'message_index','message_index',1,0,'','')
addMessageDef('length',253,'timestamp','date_time',1,0,'','')
addMessageDef('length',0,'event','event',1,0,'','')
addMessageDef('length',1,'event_type','event_type',1,0,'','')
addMessageDef('length',2,'start_time','date_time',1,0,'','')
addMessageDef('length',3,'total_elapsed_time','uint32',1000,0,'s','')
addMessageDef('length',4,'total_timer_time','uint32',1000,0,'s','')
addMessageDef('length',5,'total_strokes','uint16',1,0,'strokes','')
addMessageDef('length',6,'avg_speed','uint16',1000,0,'m/s','')
addMessageDef('length',7,'swim_stroke','swim_stroke',1,0,'swim_stroke','')
addMessageDef('length',9,'avg_swimming_cadence','uint8',1,0,'strokes/min','')
addMessageDef('length',10,'event_group','uint8',1,0,'','')
addMessageDef('length',11,'total_calories','uint16',1,0,'kcal','')
addMessageDef('length',12,'length_type','length_type',1,0,'','')
addMessageDef('length',18,'player_score','uint16',1,0,'','')
addMessageDef('length',19,'opponent_score','uint16',1,0,'','')
addMessageDef('length',20,'stroke_count','uint16',1,0,'counts','stroke_type enum used as the index')
addMessageDef('length',21,'zone_count','uint16',1,0,'counts','zone number used as the index')


addMessageDef('record',253,'timestamp','date_time',1,0,'s','')
addMessageDef('record',0,'position_lat','sint32',1,0,'semicircles','')
addMessageDef('record',1,'position_long','sint32',1,0,'semicircles','')
addMessageDef('record',2,'altitude','uint16',5,500,'m','')
addMessageDef('record',3,'heart_rate','uint8',1,0,'bpm','')
addMessageDef('record',4,'cadence','uint8',1,0,'rpm','')
addMessageDef('record',5,'distance','uint32',100,0,'m','')
addMessageDef('record',6,'speed','uint16',1000,0,'m/s','')
addMessageDef('record',7,'power','uint16',1,0,'watts','')
addMessageDef('record',8,'compressed_speed_distance','byte',100,16.0,'m/s,m','')
addMessageDef('record',9,'grade','sint16',100,0,'%','')
addMessageDef('record',10,'resistance','uint8',1,0,'','Relative. 0 is none  254 is Max.')
addMessageDef('record',11,'time_from_course','sint32',1000,0,'s','')
addMessageDef('record',12,'cycle_length','uint8',100,0,'m','')
addMessageDef('record',13,'temperature','sint8',1,0,'C','')
addMessageDef('record',17,'speed_1s','uint8',16,0,'m/s','Speed at 1s intervals.  Timestamp field indicates time of last array element.')
addMessageDef('record',18,'cycles','uint8',1,0,'cycles','')
addMessageDef('record',19,'total_cycles','uint32',1,0,'cycles','')
addMessageDef('record',28,'compressed_accumulated_power','uint16',1,0,'watts','')
addMessageDef('record',29,'accumulated_power','uint32',1,0,'watts','')
addMessageDef('record',30,'left_right_balance','left_right_balance',1,0,'','')
addMessageDef('record',31,'gps_accuracy','uint8',1,0,'m','')
addMessageDef('record',32,'vertical_speed','sint16',1000,0,'m/s','')
addMessageDef('record',33,'calories','uint16',1,0,'kcal','')
addMessageDef('record',39,'vertical_oscillation','uint16',10,0,'mm','')
addMessageDef('record',40,'stance_time_percent','uint16',100,0,'percent','')
addMessageDef('record',41,'stance_time','uint16',10,0,'ms','')
addMessageDef('record',42,'activity_type','activity_type',1,0,'','')
addMessageDef('record',43,'left_torque_effectiveness','uint8',2,0,'percent','')
addMessageDef('record',44,'right_torque_effectiveness','uint8',2,0,'percent','')
addMessageDef('record',45,'left_pedal_smoothness','uint8',2,0,'percent','')
addMessageDef('record',46,'right_pedal_smoothness','uint8',2,0,'percent','')
addMessageDef('record',47,'combined_pedal_smoothness','uint8',2,0,'percent','')
addMessageDef('record',48,'time128','uint8',128,0,'s','')
addMessageDef('record',49,'stroke_type','stroke_type',1,0,'','')
addMessageDef('record',50,'zone','uint8',1,0,'','')
addMessageDef('record',51,'ball_speed','uint16',100,0,'m/s','')
addMessageDef('record',52,'cadence256','uint16',256,0,'rpm','Log cadence and fractional cadence for backwards compatability')
addMessageDef('record',54,'total_hemoglobin_conc','uint16',100,0,'g/dL','Total saturated and unsaturated hemoglobin')
addMessageDef('record',55,'total_hemoglobin_conc_min','uint16',100,0,'g/dL','Min saturated and unsaturated hemoglobin')
addMessageDef('record',56,'total_hemoglobin_conc_max','uint16',100,0,'g/dL','Max saturated and unsaturated hemoglobin')
addMessageDef('record',57,'saturated_hemoglobin_percent','uint16',10,0,'%','Percentage of hemoglobin saturated with oxygen')
addMessageDef('record',58,'saturated_hemoglobin_percent_min','uint16',10,0,'%','Min percentage of hemoglobin saturated with oxygen')
addMessageDef('record',59,'saturated_hemoglobin_percent_max','uint16',10,0,'%','Max percentage of hemoglobin saturated with oxygen')
addMessageDef('record',62,'device_index','device_index',1,0,'','')


addMessageDef('event',253,'timestamp','date_time',1,0,'s','')
addMessageDef('event',0,'event','event',1,0,'','')
addMessageDef('event',1,'event_type','event_type',1,0,'','')
addMessageDef('event',2,'data16','uint16',1,0,'','')
addMessageDef('event',3,'data','uint32',1,0,'','')

















addMessageDef('event',4,'event_group','uint8',1,0,'','')
addMessageDef('event',7,'score','uint16',1,0,'','Do not use.  Placeholder for sport_point subfield components')
addMessageDef('event',8,'opponent_score','uint16',1,0,'','Do not use.  Placeholder for sport_point subfield components')


addMessageDef('device_info',253,'timestamp','date_time',1,0,'s','')
addMessageDef('device_info',0,'device_index','device_index',1,0,'','')
addMessageDef('device_info',1,'ant_device_type','uint8',1,0,'','')

addMessageDef('device_info',2,'manufacturer','manufacturer',1,0,'','')
addMessageDef('device_info',3,'serial_number','uint32z',1,0,'','')
addMessageDef('device_info',4,'product','uint16',1,0,'','')
addMessageDef('device_info',5,'software_version','uint16',100,0,'','')
addMessageDef('device_info',6,'hardware_version','uint8',1,0,'','')
addMessageDef('device_info',7,'cum_operating_time','uint32',1,0,'s','Reset by new battery or charge.')
addMessageDef('device_info',10,'battery_voltage','uint16',256,0,'V','')
addMessageDef('device_info',11,'battery_status','battery_status',1,0,'','')
addMessageDef('device_info',18,'sensor_position','body_location',1,0,'','Indicates the location of the sensor')
addMessageDef('device_info',19,'descriptor','string',1,0,'','Used to describe the sensor or location')
addMessageDef('device_info',20,'ant_transmission_type','uint8z',1,0,'','')
addMessageDef('device_info',21,'ant_device_number','uint16z',1,0,'','')
addMessageDef('device_info',22,'ant_network','ant_network',1,0,'','')


addMessageDef('hrv',0,'time','uint16',1000,0,'s','Time between beats')



addMessageDef('course',4,'sport','sport',1,0,'','')
addMessageDef('course',5,'name','string',1,0,'','')
addMessageDef('course',6,'capabilities','course_capabilities',1,0,'','')


addMessageDef('course_point',254,'message_index','message_index',1,0,'','')
addMessageDef('course_point',1,'timestamp','date_time',1,0,'','')
addMessageDef('course_point',2,'position_lat','sint32',1,0,'semicircles','')
addMessageDef('course_point',3,'position_long','sint32',1,0,'semicircles','')
addMessageDef('course_point',4,'distance','uint32',100,0,'m','')
addMessageDef('course_point',5,'type','course_point',1,0,'','')
addMessageDef('course_point',6,'name','string',1,0,'','')




addMessageDef('workout',4,'sport','sport',1,0,'','')
addMessageDef('workout',5,'capabilities','workout_capabilities',1,0,'','')
addMessageDef('workout',6,'num_valid_steps','uint16',1,0,'','number of valid steps')
addMessageDef('workout',8,'wkt_name','string',1,0,'','')


addMessageDef('workout_step',254,'message_index','message_index',1,0,'','')
addMessageDef('workout_step',0,'wkt_step_name','string',1,0,'','')
addMessageDef('workout_step',1,'duration_type','wkt_step_duration',1,0,'','')
addMessageDef('workout_step',2,'duration_value','uint32',1,0,'','')






addMessageDef('workout_step',3,'target_type','wkt_step_target',1,0,'','')
addMessageDef('workout_step',4,'target_value','uint32',1,0,'','')








addMessageDef('workout_step',5,'custom_target_value_low','uint32',1,0,'','')




addMessageDef('workout_step',6,'custom_target_value_high','uint32',1,0,'','')




addMessageDef('workout_step',7,'intensity','intensity',1,0,'','')



addMessageDef('schedule',0,'manufacturer','manufacturer',1,0,'','Corresponds to file_id of scheduled workout / course.')
addMessageDef('schedule',1,'product','uint16',1,0,'','Corresponds to file_id of scheduled workout / course.')

addMessageDef('schedule',2,'serial_number','uint32z',1,0,'','Corresponds to file_id of scheduled workout / course.')
addMessageDef('schedule',3,'time_created','date_time',1,0,'','Corresponds to file_id of scheduled workout / course.')
addMessageDef('schedule',4,'completed','bool',1,0,'','TRUE if this activity has been started')
addMessageDef('schedule',5,'type','schedule',1,0,'','')
addMessageDef('schedule',6,'scheduled_time','local_date_time',1,0,'','')



addMessageDef('totals',254,'message_index','message_index',1,0,'','')
addMessageDef('totals',253,'timestamp','date_time',1,0,'s','')
addMessageDef('totals',0,'timer_time','uint32',1,0,'s','Excludes pauses')
addMessageDef('totals',1,'distance','uint32',1,0,'m','')
addMessageDef('totals',2,'calories','uint32',1,0,'kcal','')
addMessageDef('totals',3,'sport','sport',1,0,'','')
addMessageDef('totals',4,'elapsed_time','uint32',1,0,'s','Includes pauses')
addMessageDef('totals',5,'sessions','uint16',1,0,'','')
addMessageDef('totals',6,'active_time','uint32',1,0,'s','')



addMessageDef('weight_scale',253,'timestamp','date_time',1,0,'s','')
addMessageDef('weight_scale',0,'weight','weight',100,0,'kg','')
addMessageDef('weight_scale',1,'percent_fat','uint16',100,0,'%','')
addMessageDef('weight_scale',2,'percent_hydration','uint16',100,0,'%','')
addMessageDef('weight_scale',3,'visceral_fat_mass','uint16',100,0,'kg','')
addMessageDef('weight_scale',4,'bone_mass','uint16',100,0,'kg','')
addMessageDef('weight_scale',5,'muscle_mass','uint16',100,0,'kg','')
addMessageDef('weight_scale',7,'basal_met','uint16',4,0,'kcal/day','')
addMessageDef('weight_scale',8,'physique_rating','uint8',1,0,'','')
addMessageDef('weight_scale',9,'active_met','uint16',4,0,'kcal/day','~4kJ per kcal, 0.25 allows max 16384 kcal')
addMessageDef('weight_scale',10,'metabolic_age','uint8',1,0,'years','')
addMessageDef('weight_scale',11,'visceral_fat_rating','uint8',1,0,'','')
addMessageDef('weight_scale',12,'user_profile_index','message_index',1,0,'','Associates this weight scale message to a user.  This corresponds to the index of the user profile message in the weight scale file.')



addMessageDef('blood_pressure',253,'timestamp','date_time',1,0,'s','')
addMessageDef('blood_pressure',0,'systolic_pressure','uint16',1,0,'mmHg','')
addMessageDef('blood_pressure',1,'diastolic_pressure','uint16',1,0,'mmHg','')
addMessageDef('blood_pressure',2,'mean_arterial_pressure','uint16',1,0,'mmHg','')
addMessageDef('blood_pressure',3,'map_3_sample_mean','uint16',1,0,'mmHg','')
addMessageDef('blood_pressure',4,'map_morning_values','uint16',1,0,'mmHg','')
addMessageDef('blood_pressure',5,'map_evening_values','uint16',1,0,'mmHg','')
addMessageDef('blood_pressure',6,'heart_rate','uint8',1,0,'bpm','')
addMessageDef('blood_pressure',7,'heart_rate_type','hr_type',1,0,'','')
addMessageDef('blood_pressure',8,'status','bp_status',1,0,'','')
addMessageDef('blood_pressure',9,'user_profile_index','message_index',1,0,'','Associates this blood pressure message to a user.  This corresponds to the index of the user profile message in the blood pressure file.')



addMessageDef('monitoring_info',253,'timestamp','date_time',1,0,'','')
addMessageDef('monitoring_info',0,'local_timestamp','local_date_time',1,0,'','Use to convert activity timestamps to local time if device does not support time zone and daylight savings time correction.')


addMessageDef('monitoring',253,'timestamp','date_time',1,0,'s','Must align to logging interval, for example, time must be 00:00:00 for daily log.')
addMessageDef('monitoring',0,'device_index','device_index',1,0,'','Associates this data to device_info message.  Not required for file with single device (sensor).')
addMessageDef('monitoring',1,'calories','uint16',1,0,'kcal','total calories')
addMessageDef('monitoring',2,'distance','uint32',100,0,'m','')
addMessageDef('monitoring',3,'cycles','uint32',2,0,'cycles','')
addMessageDef('monitoring',4,'active_time','uint32',1000,0,'s','')
addMessageDef('monitoring',5,'activity_type','activity_type',1,0,'','')
addMessageDef('monitoring',6,'activity_subtype','activity_subtype',1,0,'','')
addMessageDef('monitoring',8,'distance_16','uint16',1,0,'100 * m','')
addMessageDef('monitoring',9,'cycles_16','uint16',1,0,'2 * cycles (steps)','')
addMessageDef('monitoring',10,'active_time_16','uint16',1,0,'s','')
addMessageDef('monitoring',11,'local_timestamp','local_date_time',1,0,'','Must align to logging interval, for example, time must be 00:00:00 for daily log.')



addMessageDef('memo_glob',250,'part_index','uint32',1,0,'','Sequence number of memo blocks')
addMessageDef('memo_glob',0,'memo','byte',1,0,'','Block of utf8 bytes ')
addMessageDef('memo_glob',1,'message_number','uint16',1,0,'','Allows relating glob to another mesg  If used only required for first part of each memo_glob')
addMessageDef('memo_glob',2,'message_index','message_index',1,0,'','Index of external mesg')




